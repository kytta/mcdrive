# mcdrive

> Drive-in management for fast food restaurants

This is an educational project. It is not affiliated with McDonalds (please
don't sue me)

## Licence

[Unlicense](https://spdx.org/licenses/Unlicense.html) — released into the public domain.
